;;; packages.el --- yos layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author: Tomoaki Yoshida(T440s) <yos@VMXubuntu16C>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3


;;; Code:

(defconst howm-packages
  '(
    howm
    )
  "The list of Lisp packages required by the howm layer.")

(defun howm/init-howm ()
  (use-package howm
    :init
    (setq howm-menu-lang 'ja)
    (setq howm-view-title-header "*" )
    (setq howm-prefix "\C-ch")
    :config
    (setq howm-file-name-format (concat "%Y/%m/%Y_%m_%d-" (system-name) ".howm"))
    (setq howm-keyword-case-fold-search t) ; <<< で大文字小文字を区別しない
    (setq howm-list-title nil) ; 一覧時にタイトルを表示しない
    (setq howm-menu-refresh-after-save nil) ; save 時にメニューを自動更新せず
    (setq howm-refresh-after-save nil) ; save 時に下線を引き直さない
    (setq howm-menu-expiry-hours 2) ; メニューを 2 時間キャッシュ
    (setq howm-view-use-grep t) ;grep 使うよ
    (setq howm-view-grep-command "rg") ;ripgrep使うよ
    ))

;;; packages.el ends here
