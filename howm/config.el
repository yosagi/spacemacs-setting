;; howm + org
(with-eval-after-load 'org
(add-hook 'org-mode-hook 'howm-mode)
(add-to-list 'auto-mode-alist '("\\.howm$" . org-mode))
) ;; with-eval-after-load 'org

