(setq default-input-method "W32-IME")

; modeline の関係からか動かない
;(when (locate-library "w32-ime")
;   (setq-default w32-ime-mode-line-state-indicator "[--]")
;   (setq w32-ime-mode-line-state-indicator-list '("[--]" "[あ]" "[--]"))
;   (w32-ime-initialize)
;)

; ミニバッファに移動した際は最初に日本語入力が無効な状態にする
(add-hook 'minibuffer-setup-hook 'deactivate-input-method)

;; isearch に移行した際に日本語入力を無効にする
(add-hook 'isearch-mode-hook '(lambda ()
                                (deactivate-input-method)
                                (setq w32-ime-composition-window
                                      (minibuffer-window))))
(add-hook 'isearch-mode-end-hook '(lambda ()
                                    (setq w32-ime-composition-window nil)))

;;   ;; helm 使用中に日本語入力を無効にする
(advice-add 'helm :around '(lambda (orig-fun &rest args)
                             (let ((select-window-functions nil)
                                   (w32-ime-composition-window
                                    (minibuffer-window)))
                               (deactivate-input-method)
                               (apply orig-fun args))))
