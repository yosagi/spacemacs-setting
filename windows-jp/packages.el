;;; packages.el --- windows-jp layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author:  <yos@YOS-VAIOE>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:
;;; Code:

(defconst windows-jp-packages
  '(ripgrep)
  "The list of Lisp packages required by the windows-jp layer.")


(defun windows-jp/init-ripgrep ()
  (use-package ripgrep))


;;; packages.el ends here
