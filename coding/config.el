(with-eval-after-load 'google-c-style)
(c-add-style "google-yosmod"
             '("google"
               (c-basic-offset . 2)
               (c-offsets-alist
                (access-label . 0)
                (class-close . 0)
                (inclass . +)
                (topmost-intro . +)
                )))
