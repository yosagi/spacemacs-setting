;;; packages.el --- yos layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author: Tomoaki Yoshida(T440s) <yos@VMXubuntu16C>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3


;;; Code:

(defconst coding-packages
  '(
    google-c-style
    rg
    )
  "The list of Lisp packages required by the howm layer.")

(defun coding/init-rg ()
  (use-package rg
    :init
    :config
    ))
(defun coding/init-google-c-style ()
  (use-package google-c-style
    :config
    (add-hook 'c-mode-common-hook 'google-set-c-style)
    (add-hook 'c++-mode-common-hook 'google-set-c-style)
    (add-hook 'c-mode-common-hook 'google-make-newline-indent)
    ))
;;; packages.el ends here
