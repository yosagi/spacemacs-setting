(defun org-memo/helm-ag--init-sjis ()
  (let ((buf-coding buffer-file-coding-system))
    (helm-attrset 'recenter t)
    (with-current-buffer (helm-candidate-buffer 'global)
      (let* ((default-directory (or helm-ag--default-directory
                                    default-directory))
             (cmds (helm-ag--construct-command (helm-attr 'search-this-file)))
             (coding-system-for-read buf-coding)
             (coding-system-for-write 'sjis))
        (setq helm-ag--ignore-case (helm-ag--ignore-case-p cmds helm-ag--last-query)
              helm-ag--last-command cmds)
        (let ((ret (apply #'process-file (car cmds) nil t nil (cdr cmds))))
          (if (zerop (length (buffer-string)))
              (error "No ag output: '%s'" helm-ag--last-query)
            (unless (zerop ret)
              (unless (executable-find (car cmds))
                (error "'ag' is not installed."))
              (error "Failed: '%s'" helm-ag--last-query))))
        (when helm-ag--buffer-search
          (helm-ag--abbreviate-file-name))
        (helm-ag--remove-carrige-returns)
        (helm-ag--save-current-context)))))


(defun org-memo/journal-file-name ()
  (interactive)
  "returns formatted file name"
  (format-time-string
   (concat org-memo-directory org-memo-file-format ))
)

(defun org-memo/open-journal ()
  (interactive)
  "open the journal file"
  (find-file (org-memo/journal-file-name))
)

(defun org-memo/search-journal ()
  "Search in journals with a search tool [(org-memo-search-cmd)]."
  (interactive)
  (let ((helm-ag-base-command org-memo-search-cmd))
        (dflet ((helm-ag--init () (org-memo/helm-ag--init-sjis)))
    (helm-ag org-memo-directory)
    )))

(defun org-memo/list-journal ()
  (interactive)
  "list journal files"
  (neotree-dir org-memo-directory)
  (let ((month (format-time-string "%m"))
        (year  (format-time-string "%Y")))
    (search-forward year)
    (neotree-enter)
    (search-forward month)
    (neotree-enter)
    (neotree-select-previous-sibling-node)
    (neotree-quick-look)
    )
)

(defun org-memo/find-archive ()
  (interactive)
  ""
  (if (string= major-mode "org-mode")
      (find-file
       (format
        (car (split-string org-archive-location "::")) buffer-file-name))
    )
)
