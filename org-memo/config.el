;; howm like org memo
(defvar org-memo-directory "~/Sync/memo")
(defvar org-directory org-memo-directory)
(defvar org-agenda-files (list org-memo-directory))
(defvar org-memo-file-format (concat "/%Y/%m/%Y_%m_%d-" (system-name) ".org"))
;(defvar org-memo-search-cmd "pt -e --nocolor --nogroup")
(defvar org-memo-search-cmd "rg --vimgrep --no-heading --sort-files -i")
(defvar org-startup-folded nil)
(defvar org-startup-truncated nil)
(defvar org-capture-templates
      '(("." "Today's log" entry
         (file org-memo/journal-file-name)
         "* %?\n %T  %i\n  %a")
        )
      )

(with-eval-after-load 'org
  ;; howm file は org-mode で開く
  (add-to-list 'auto-mode-alist '("\\.howm$" . org-mode))

  ;; fill-column
  (add-hook 'org-mode-hook (lambda () (setq fill-column 72)))

  ;; load capture template config
  (let ((captureconffile (concat org-memo-directory "/capture-config.el")))
    (if (file-readable-p captureconffile)
        (load-file captureconffile)
      )
    )

) ;; with-eval-after-load 'org

