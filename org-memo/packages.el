;;; packages.el --- yos layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author: Tomoaki Yoshida(T440s) <yos@VMXubuntu16C>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3


;;; Code:

(defconst org-memo-packages
  '(
;    ripgrep
    org-seek
    open-junk-file
    el-x
    )
  "The list of Lisp packages required by the org-memo layer.")

;(defun org-memo/init-ripgrep ()
;  (use-package ripgrep))

(defun org-memo/init-el-x ()
  (use-package el-x))

(defun org-memo/init-org-seek ()
  (use-package org-seek
    :config
    (setq org-seek-search-tool 'ripgrep)
    (setq org-seek-org-root-path org-memo-directory)
    )
  (org-version)
)

;; (defun org-memo/init-open-junk-file ()
;;   (use-package open-junk-file
;;     :config
;;     (setq open-junk-file-format
;;           (concat "~/Sync/memo/%Y/%m/%Y_%m_%d-" (system-name) ".org")))
;;   ;; spacemacs/spacemacs-ui より
;;   (spacemacs/set-leader-keys "fJ" 'spacemacs/open-junk-file))


;;; packages.el ends here
