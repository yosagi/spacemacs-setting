; howm like keybinding
(global-set-key (kbd "C-c h .") 'org-memo/open-journal)
(global-set-key (kbd "C-c h s") 'org-memo/search-journal)
(global-set-key (kbd "C-c h a") 'org-memo/list-journal)
(global-set-key (kbd "C-c h o") 'org-memo/find-archive)


